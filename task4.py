class Book():
    def __init__(self, author, title, publishingOffice, year, numberOfPages):
        self.author = author
        self.title = title
        self.publishingOffice = publishingOffice
        self.year = year
        self.numberOfPages = numberOfPages