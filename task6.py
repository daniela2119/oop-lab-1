class Train():
    def __init__(self, destination, number, departureTime, arrayOfFreePlaces):
        self.destination = destination
        self.number = number
        self.departureTime = departureTime
        self.numberOfGeneralPlaces = arrayOfFreePlaces[0]
        self.numberOfReservedPlaces = arrayOfFreePlaces[1]
        self.numberOfCompartmentPlaces = arrayOfFreePlaces[2]