class Computer():
    def __init__(self, CPU, motherboard, hardDiscSpace, RAMSize, graphicsCard, isDVDDrive):
        self.CPU = CPU
        self.motherboard = motherboard
        self.hardDiscSpace = hardDiscSpace
        self.RAMSize = RAMSize
        self.graphicsCard = graphicsCard
        self.isDVDDrive = isDVDDrive