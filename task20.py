class Scanner():
    def __init__(self, model, producer, price, width, height, opticalResolution, grayGradationsNumber):
        self.model = model
        self.producer = producer
        self.price = price
        self.width = width
        self.height = height
        self.opticalResolution = opticalResolution
        self.grayGradationsNumber = grayGradationsNumber