class Bus():
    def __init__(self, surname, initials, busNumber, wayNumber, model, runningYear, mileage):
        self.surname = surname
        self.initials = initials
        self.busNumber = busNumber
        self.wayNumber = wayNumber
        self.model = model
        self.runningYear = runningYear
        self.mileage = mileage